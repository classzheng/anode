// Anode.h
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "Anode-wget.h"
#define PRTTAG(TAG) printf(" \033[47m\033[30m %s \033[37m\033[40m",TAG);
typedef enum {
	etitle ,
	etext  ,
	eimage ,
	escript,
} eletype;
enum {
	INCORRECT_DOMAIN=1,
} _EXCEPT_VAL;
typedef struct {
	unsigned id;
	char*	 script;
	eletype  etype;
} element;
typedef struct exrnd {
	website* web;
	element* elis;
	unsigned tokb,toke;
	unsigned lisl;
	void(*next)(struct exrnd*);
	void(*pars)(struct exrnd*);
	void(*init)(struct exrnd*);
	void(*rend)(struct exrnd*);
} render;
typedef struct {
	char domain[20];
	char ip_add[20];
} dmp2p;
typedef struct exand{
	render aren;
	dmp2p  dmpn[256];
	short  p2pl;
	Bool(*vist)(struct exand*,const char*,unsigned);
	Bool(*vbip)(struct exand*,const char*,unsigned);
	void(*init)(render*,struct exand*);
} anode;
char*substr(const char*str,int beg,int end) {
	char*sub=(char*)malloc((end-beg+1)*sizeof(char));
	for(unsigned i = 0; i<(end-beg+1); i++) {
		sub[i]=str[i+beg];
	}
	return sub;
}
void stdnext(struct exrnd*anode) { // Next token
	anode->tokb=anode->toke+1;
	char first_spc=1;
	for(unsigned i = anode->tokb; i < strlen(anode->web->raw_data); i++) {
		if(anode->web->raw_data[i]=='>'&&anode->web->raw_data[i-1]!='@') {
			anode->toke=i;
			break;
		} else if((anode->web->raw_data[i]==' '||anode->web->raw_data[i]=='\n'||anode->web->raw_data[i]=='\r')&&first_spc) {
			++anode->tokb;
		} else {
			first_spc=0;
		}
	}
	if(anode->tokb==1) anode->tokb--;
	return ;
}
void stdpars(struct exrnd*anode) {
	char*code=anode->web->raw_data,*etyp=(char*)malloc(20*sizeof(char));
	element temp;
	while(anode->toke!=(strlen(anode->web->raw_data)-1)) {
		anode->next(anode);
		sscanf(substr(code,anode->tokb,anode->toke),"%[^:]",etyp);
		if(!strcmp(etyp,"@Title")) {
			temp.etype=etitle;
		} else if(!strcmp(etyp,"@Text")) {
			temp.etype=etext;
		} else if(!strcmp(etyp,"@Image")) {
			temp.etype=eimage;
		} else if(!strcmp(etyp,"@Script")) {
			temp.etype=escript;
		}
		for(unsigned i = anode->tokb+strlen(etyp); ; i++) {
			if(code[i]=='<') {
				temp.script=substr(code,i+1,anode->toke-1);
				break;
			}
		}
		anode->elis=(element*)malloc((++(anode->lisl))*sizeof(element));
		temp.id++;
	}
	return ;
}
void stdinit_r(struct exrnd*anode) {
	anode->web=(website*)malloc(sizeof(website));
	anode->elis=(element*)NULL;
	anode->lisl=0;
	return ;
}
void stdrend(struct exrnd*anode) {
	printf("\033[0m\033c"); // The `Esc` code
	PRTTAG("Id") printf(":%u",anode->web->id);
	PRTTAG("Server") printf(":%s",anode->web->server);
	PRTTAG("Domain") printf(":%s",anode->web->domain),printf("\r\n");
	anode->pars(anode);
	return ;
}
Bool stdvist(struct exand*anode,const char*domain,unsigned port) {
	for(unsigned i = 0; i != anode->p2pl; i++) {
		if(!strcmp(anode->dmpn[i].domain,domain)) {
			anode_wget(anode->aren.web,anode->dmpn[i].ip_add,port);
			anode->aren.pars(&anode->aren);
			anode->aren.rend(&anode->aren);
			return True;
		}
	}
	return False;
}
Bool stdvbip(struct exand*anode,const char*ipad,unsigned port) {
	anode_wget(anode->aren.web,ipad,port);
	anode->aren.pars(&anode->aren);
	anode->aren.rend(&anode->aren);
	return True;
}
void stdinit_a(render*rndr,struct exand*anode) {
	rndr->next=stdnext;
	rndr->pars=stdpars;
	rndr->init=stdinit_r;
	rndr->rend=stdrend;
	(*rndr).init(rndr);
	anode->p2pl=0;
	anode->vist=stdvist;
	anode->vbip=stdvbip;
	return ;
}
