// Anode.c
#include "Anode.h"
int main(void) {
	TRY() {
		anode browser;
		browser.init=stdinit_a;
		browser.init(&browser.aren,&browser);
		if(!browser.vbip(&browser,lochost,defport));
		return 0;
	} CATCH(CONNECT_FAILED) {
		ANODE_LOG("[@Anode-wget logout] Connect failed.%s","\r\n");
	} CATCH(INCORRECT_IP) {
		ANODE_LOG("[@Anode-wget logout] Incorrect IP.%s","\r\n");
	} CATCH(UNSAFE_PORT) {
		ANODE_LOG("[@Anode-wget logout] Unsafe port.%s","\r\n");
	} CATCH(INCORRECT_DOMAIN) {
		ANODE_LOG("[@Anode-wget logout] Incorrect domain.%s","\r\n");
	} FINALLY() {
		ANODE_LOG("[@Anode logout] Report https://gitee.com/e_fixed/anode/issues. %s","\r\n");
	}
	return 0;
}
