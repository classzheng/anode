// Anode-wget.h
#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include <setjmp.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#ifdef NDEBUG
# define ANODE_LOG(...) return 0;
#else
# define ANODE_LOG(...) fprintf(stderr,__VA_ARGS__);
#endif
#define TRY() int temp_jbr=setjmp(except);if(!temp_jbr)
#define CATCH(VAL) else if(temp_jbr==VAL)
#define THROW(VAL) longjmp(except,VAL);
#define FINALLY() if(1)
#define LOCALHOST    ((const char*)"127.0.0.1")
#define DEFAULT_PORT ((unsigned)23)
#ifndef DEBUG
# define lochost LOCALHOST
# define defport DEFAULT_PORT
#endif
typedef enum {
	False,
	True,
} Bool;
typedef struct {
	unsigned    id;
	long double sign;
	const char*	server;
	char        domain[24];
	char        raw_data[1024*1024];
} website;
enum {
	NORMAL,
	CONNECT_FAILED=4,
	INCORRECT_IP, UNSAFE_PORT
} EXCEPT_VAL;
typedef struct sockaddr_in sockaddr_in;
sockaddr_in an_serv;
jmp_buf except;
Bool anode_wget(website* web,const char* serv_ip,unsigned port) {
	web->server=serv_ip;
	int sock=socket(AF_INET,SOCK_STREAM,0);
	memset(&an_serv,0,sizeof(an_serv));
	an_serv.sin_family=AF_INET;
	an_serv.sin_addr.s_addr=inet_addr(serv_ip);
	an_serv.sin_port=htons(port);
	int a,b,c,d;
	if(sscanf(serv_ip,"%d.%d.%d.%d",&a,&b,&c,&d)<4) THROW(INCORRECT_IP);
	if((a<0||a>255)+(b<0||b>255)+(c<0||c>255)+(d<0||d>255)>0) THROW(INCORRECT_IP);
	if(port<0&&port>5000) THROW(UNSAFE_PORT);
	int cnct=(connect(sock,(const struct sockaddr_in*)&an_serv,sizeof(an_serv)));
	if(cnct<0) {
		THROW(CONNECT_FAILED);
	}
	recv(sock,web,sizeof(*web),0);
	return True;
}
