## Anode项目
### 项目简介
一个开源的C语言网络排版器，基于LINUX SOCKET，使用特殊的协议进行交流（No GNU/Wget），并使用特殊的标记语言来网页排版。

<img src="https://gitee.com/e_fixed/anode/raw/releaser/Anode.jpg" weight="200%" height="200%"/>

```c
// Anode.c
#include "Anode.h"
int main(void) {
	TRY() {
		anode browser;
		browser.init=stdinit_a;
		browser.init(&browser.aren,&browser);
		if(!browser.vist(&browser,"anode:home",defport)) {
			THROW(INCORRECT_DOMAIN);
		}
		return 0;
	} CATCH(CONNECT_FAILED) {
		ANODE_LOG("[@Anode-wget logout] Connect failed.%s","\r\n");
	} CATCH(INCORRECT_IP) {
		ANODE_LOG("[@Anode-wget logout] Incorrect IP.%s","\r\n");
	} CATCH(UNSAFE_PORT) {
		ANODE_LOG("[@Anode-wget logout] Unsafe port.%s","\r\n");
	} CATCH(INCORRECT_DOMAIN) {
		ANODE_LOG("[@Anode-wget logout] Incorrect domain.%s","\r\n");
	} FINALLY() {
		ANODE_LOG("[@Anode logout] Report https://gitee.com/e_fixed/anode. %s","\r\n");
	}
	return 0;
}


```

### 获取方法
1.单击【克隆/下载▾】按钮

2.选择【下载ZIP】或者复制代码 https://gitee.com/e_fixed/anode.git

3.打开Terminal，并输入：
```shell
git clone https://gitee.com/e_fixed/anode.git
```
> 本项目仅支持在UNIX/LINUX系统下运行
### 创建自己的网站
> 敬请期待